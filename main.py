from odpslides.presentation import Presentation
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLabel, QGridLayout
from PyQt5.QtWidgets import QLineEdit, QPushButton, QHBoxLayout, QPlainTextEdit, QButtonGroup, QGroupBox
from PyQt5.QtCore import pyqtSlot
import sys


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Editor'
        self.left = 100
        self.top = 100
        self.width = 420
        self.height = 400
        self.p = Presentation(background_color='darkseagreen', footer="footer")
        self.title_of_slide = QPlainTextEdit(self)
        self.subtitle_of_slide = QPlainTextEdit(self)
        self.text_of_slide = QPlainTextEdit(self)
        self.name_of_image = QPlainTextEdit(self)
        self.name_of_presentation = QPlainTextEdit(self)
        self.init_ui()

    def create_button(self, name, tip, pos_x, pos_y, func_name):
        button_title = QPushButton(name, self)
        button_title.setToolTip(tip)
        button_title.move(pos_x, pos_y)
        button_title.clicked.connect(func_name)

    def init_ui(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # creating buttons
        self.create_button('Add title slide', 'Click to add new title slide', 20, 360, self.title_slide)
        self.create_button('Add text slide', 'Click to add new text slide', 120, 360, self.text_slide)
        self.create_button('Add image slide', 'Click to add new image slide', 220, 360, self.image_slide)
        self.create_button('save and exit', 'Click to save and exit', 320, 360, self.save_exit)

        # positioning buttons
        self.name_of_presentation.move(20, 20)
        self.name_of_presentation.resize(200, 24)

        self.title_of_slide.move(20, 50)
        self.title_of_slide.resize(200, 24)

        self.subtitle_of_slide.move(20, 80)
        self.subtitle_of_slide.resize(200, 24)

        self.text_of_slide.move(20, 110)
        self.text_of_slide.resize(300, 200)

        self.name_of_image.move(20, 320)
        self.name_of_image.resize(200, 24)

        self.show()

    def title_slide(self):
        self.p.add_title_chart(title=self.title_of_slide.toPlainText(), subtitle=self.subtitle_of_slide.toPlainText())

    def text_slide(self):
        self.p.add_titled_outline_chart(title=self.title_of_slide.toPlainText(), outline=self.text_of_slide.toPlainText())

    def image_slide(self):
        self.p.add_titled_image(title=self.title_of_slide.toPlainText(), image_file=self.name_of_image.toPlainText(),
                                pcent_stretch_center=80, pcent_stretch_content=80)

    def save_exit(self):
        self.p.save(filename=self.name_of_presentation.toPlainText(), launch=1)
        sys.exit()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
